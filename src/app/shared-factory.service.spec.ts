import { TestBed } from '@angular/core/testing';

import { SharedFactoryService } from './shared-factory.service';

describe('SharedFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedFactoryService = TestBed.get(SharedFactoryService);
    expect(service).toBeTruthy();
  });
});
