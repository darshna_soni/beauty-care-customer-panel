import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LoginComponent } from './auth/login/login.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { LoginRegistrationComponent } from './auth/login-registration/login-registration.component';
import { HeaderComponent } from './pages/header/header.component';
import { HeaderImageComponent } from './pages/header-image/header-image.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { AdvanceSearchComponent } from './pages/advance-search/advance-search.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { FooterComponent } from './pages/footer/footer.component';
import { ProductSearchContainerComponent } from './pages/product-search-container/product-search-container.component';
import { CartComponent } from './pages/cart/cart.component';
import { Ng5SliderModule } from 'ng5-slider';
import { BillComponent } from './pages/bill/bill.component';
import { StarRatingComponent } from './pages/star-rating/star-rating.component';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    LoginRegistrationComponent,
    HeaderComponent,
    HeaderImageComponent,
    ProductListComponent,
    AdvanceSearchComponent,
    PortfolioComponent,
    FooterComponent,
    ProductSearchContainerComponent,
    CartComponent,
    BillComponent,
    StarRatingComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    AngularFontAwesomeModule,
    Ng5SliderModule,
    FormsModule,
    HttpClientModule,
    PerfectScrollbarModule
  ],
  providers: [{
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  bootstrap: [AppComponent, LoginComponent, RegistrationComponent]
})
export class AppModule { }
