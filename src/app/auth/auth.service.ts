import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

const AUTH_URL = `${environment.SERVER_URL}/auth`;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
   
  signUp(userData:any) {
    return this.http
      .post(`${AUTH_URL}/signup/`,userData).pipe(
        map((response: Response) => {
          return response;
        }));
  }

  login(userData){
    return this.http
      .post(`${AUTH_URL}/login/`,userData).pipe(
        map((response: Response) => {
          return response;
        }));
  }
}
