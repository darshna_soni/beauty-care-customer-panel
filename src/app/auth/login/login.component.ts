import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../auth.service';
import { SharedFactoryService } from 'src/app/shared-factory.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userData={
     email: '',
     password: ''
  }
  constructor(private activeModal: NgbActiveModal, private authService: AuthService, private loginUser: SharedFactoryService) { }

  ngOnInit() {
  }
  
  login() {
    this.authService.login(this.userData).subscribe(
      (result: any) => {
        localStorage.setItem("token", result.accessToken);
        this.loginUser.setToken(result.accessToken);
        this.activeModal.close();
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }
}
