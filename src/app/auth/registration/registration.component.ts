import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  userData = {
    name: '',
    email: '',
    password: '',
    rePassword: ''
  }
  constructor(
    private activeModal: NgbActiveModal,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }
  signUp() {
    this.authService.signUp(this.userData).subscribe(
      (result: any) => {
        this.activeModal.dismiss();
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }
}
