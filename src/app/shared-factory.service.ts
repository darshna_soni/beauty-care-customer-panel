import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedFactoryService {
  private authenticatedUserInfo: any = {};
  private token: String = "";

  constructor() { }

  getAuthenticatedUserInfo() {
    return this.authenticatedUserInfo;
  }

  setAuthenticatedUserInfo(authenticatedUserInfo: any) {
    this.authenticatedUserInfo = authenticatedUserInfo;
  }

  private getUserType(){
    return this.authenticatedUserInfo.type;
  }

  getImagePath(){
    return this.authenticatedUserInfo.imagePath;
  }

  getEmail(){
    return this.authenticatedUserInfo.email;
  }

  getId(){
    return this.authenticatedUserInfo.id;
  }

  getName(){
    return this.isSuperAdmin() ? 
           this.authenticatedUserInfo.fullName:
           this.authenticatedUserInfo.parlorName;
  }

  isSuperAdmin(){
   return this.getUserType() == 'Super Admin';  
  }

  isAdmin(){
    return this.getUserType() == 'Admin';  
  }

  setToken(token: String){
    return this.token = token;
  }

  getToken(){
    return this.token;  
  }
}
