import { Component, OnInit } from '@angular/core';
import { SharedFactoryService } from './shared-factory.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'customer-panel';

  constructor(private loginUser: SharedFactoryService) { }

  ngOnInit() {
    this.loginUser.setToken(localStorage.getItem("token"));
  }
}
