import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { RegistrationComponent } from 'src/app/auth/registration/registration.component';
import { Router } from '@angular/router';
import { SharedFactoryService } from 'src/app/shared-factory.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private modalService: NgbModal, private router: Router, private loginUser: SharedFactoryService) { }

  token: String;
  isTokenEmpty: boolean;

  ngOnInit() {
    console.log(this.loginUser.getToken());
    this.token = this.loginUser.getToken();
    this.isTokenEmpty = this.token == undefined || this.token == '';
  }

  openLoginModal() {
    const modalRef = this.modalService.open(LoginComponent, {
      windowClass: 'dark-modal width300'
    });
    modalRef.result.then((result) => { }, (error) => {
      console.log('Error', error);
    });
  }

  openRegistrationModal() {
    const modalRef = this.modalService.open(RegistrationComponent, {
      windowClass: 'dark-modal width300'
    });
    modalRef.result.then((result) => { }, (error) => {
      console.log('Error', error);
    });

  }

  logout() {
    localStorage.removeItem("token");
    this.router.navigate(['']);
    this.isTokenEmpty = true;
  }

}
