import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSearchContainerComponent } from './product-search-container.component';

describe('ProductSearchContainerComponent', () => {
  let component: ProductSearchContainerComponent;
  let fixture: ComponentFixture<ProductSearchContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSearchContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSearchContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
