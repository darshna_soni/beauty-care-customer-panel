import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-search-container',
  templateUrl: './product-search-container.component.html',
  styleUrls: ['./product-search-container.component.scss']
})
export class ProductSearchContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
