import { Component, OnInit, Injectable } from '@angular/core';
import { ProductService } from './product.service';
import { Router } from '@angular/router';
import { SharedFactoryService } from 'src/app/shared-factory.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { CartService } from '../cart/cart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService,
    private router: Router, 
    private loginUser: SharedFactoryService, 
    private cartService: CartService,
    private modalService: NgbModal
  ) { }

  itemList: any;

  ngOnInit() {
    this.getAllItem(0);
  }

  getAllItem(itemId) {
    this.productService.getAllItem(itemId).subscribe(
      (result: any) => {
        this.itemList = result;
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }

  isLoggedInUser(salonItemId: number, parlorId: number) {

    if (this.loginUser.getToken() == undefined || this.loginUser.getToken() == '') {
      const modalRef = this.modalService.open(LoginComponent, {
        windowClass: 'dark-modal width300'
      });
      modalRef.result.then((result) => {
        this.addToCart(salonItemId, parlorId);
      }, (error) => {
        console.log('Error', error);
      });
    }
    else {
      this.addToCart(salonItemId, parlorId);
    }

  }


  addToCart(salonItemId: number, parlorId: number) {
    this.cartService.addToCart(salonItemId, parlorId).subscribe(
      (result: any) => {
        this.router.navigate(['/cart']);
      },
      (error) => {
        console.log('Error', error);
      }
    );

  }

}
