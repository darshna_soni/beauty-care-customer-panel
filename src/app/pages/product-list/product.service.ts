import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { SharedFactoryService } from 'src/app/shared-factory.service';

const ITEM_URL = `${environment.SERVER_URL}/item`;


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'Bearer ' + localStorage.getItem("token"));
  constructor(private http: HttpClient, private loginUser: SharedFactoryService) { 
    
  }

  getAllItem(itemId: number) {
    return this.http
      .post(`${ITEM_URL}/getAllItem/`, { itemId: itemId }).pipe(
        map((response: Response) => {
          return response;
        }));
  }

  addToCart(salonItemId: number, parlorId: number) {
    return this.http
      .get(`${ITEM_URL}/addToCart/` + salonItemId + '/' + parlorId, { headers: this.headers }).pipe(
        map((response: Response) => {
          return response;
        }));
  }
}
