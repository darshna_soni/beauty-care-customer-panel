import { Component, OnInit, Input } from '@angular/core';
import { LabelType, Options } from 'ng5-slider';
import { AdvanceSearchService } from './advance-search.service';
import { ProductListComponent } from '../product-list/product-list.component';

@Component({
  selector: 'app-advance-search',
  templateUrl: './advance-search.component.html',
  styleUrls: ['./advance-search.component.scss']
})
export class AdvanceSearchComponent implements OnInit {
  minValue: number = 100;
  maxValue: number = 400;

  categoryList: any;
  itemList: any;
  options: Options = {
    floor: 0,
    ceil: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min price:</b> Rs' + value;
        case LabelType.High:
          return '<b>Max price:</b> Rs' + value;
        default:
          return "" + value;
      }
    }
  };
  constructor(private advanceSearchService: AdvanceSearchService) { }

  ngOnInit() {
    this.getAllCategory();

  }
  getAllCategory() {
    this.advanceSearchService.getAllCategory().subscribe(
      (result: any) => {
        this.categoryList = result;
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }

  getAllItem(categoryId: number) {
    this.advanceSearchService.getAllItem(categoryId).subscribe(
      (result: any) => {
        this.itemList = result;
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }

  /* filterItem(itemId: number) {
    this.productList.getAllItem(itemId);
  } */

}
