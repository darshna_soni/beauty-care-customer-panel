import { TestBed } from '@angular/core/testing';

import { AdvanceSearchService } from './advance-search.service';

describe('AdvanceSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvanceSearchService = TestBed.get(AdvanceSearchService);
    expect(service).toBeTruthy();
  });
});
