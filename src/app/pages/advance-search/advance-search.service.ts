import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const ADVANCE_URL = `${environment.SERVER_URL}/filter`;

@Injectable({
  providedIn: 'root'
})
export class AdvanceSearchService {

  constructor(private http: HttpClient) { }

  getAllCategory(){
    return this.http
      .get(`${ADVANCE_URL}/getAllCategory`).pipe(
        map((response: Response) => {
          return response;
        }));
  }
  getAllItem(categoryId:number){
    return this.http
      .get(`${ADVANCE_URL}/getAllItem/`+categoryId).pipe(
        map((response: Response) => {
          return response;
        }));
  }

  
}
