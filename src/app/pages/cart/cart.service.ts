import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedFactoryService } from 'src/app/shared-factory.service';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const CART_URL = `${environment.SERVER_URL}/cart`;
@Injectable({
  providedIn: 'root'
})
export class CartService {
  private headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'Bearer ' + localStorage.getItem("token"));
  constructor(private http: HttpClient, private loginUser: SharedFactoryService) { }

  addToCart(salonItemId: number, parlorId: number) {
    return this.http
      .get(`${CART_URL}/addToCart/` + salonItemId + '/' + parlorId, { headers: this.headers }).pipe(
        map((response: Response) => {
          return response;
        }));
  }

  getCartItem() {
    return this.http
      .get(`${CART_URL}/getCartItem/`, { headers: this.headers }).pipe(
        map((response: Response) => {
          return response;
        }));
  }
}
