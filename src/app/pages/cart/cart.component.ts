import { Component, OnInit } from '@angular/core';
import { CartService } from './cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartList: any;
  totalAmount = 0;
  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.getCartItem();
  }

  getCartItem() {
    this.cartService.getCartItem().subscribe(
      (result: any) => {
        this.cartList = result;
        this.totalAmount = this.cartList.reduce((sum, cart) => sum + (cart.price * cart.quantity), 0);
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }

  changeCartTotal(){
    this.totalAmount = this.cartList.reduce((sum, cart) => sum + (cart.price * cart.quantity), 0);
  }
}
